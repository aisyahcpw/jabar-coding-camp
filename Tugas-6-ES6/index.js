// soal no 1
let  luaspersegipanjang = (p,l) => { return p*l;};
console.log('Luas Persegi Panjang =',luaspersegipanjang(10,5));
luaspersegipanjang()

let  kelilingpersegipanjang = (p,l) => { return p+p+l+l;};
console.log('Keliling Persegi Panjang =',kelilingpersegipanjang(10,5));
kelilingpersegipanjang()

// soal no 2
const newFunction = (firstName, lastName) => {
  return {
    firstName,
    lastName,
    fullName: function(){
      console.log(firstName + " " + lastName)
    }
  }
}
//Driver Code 
newFunction("William", "Imoh").fullName() 

//revisi no 2
const revisinewFunction = (firstName, lastName) => {
  return {
    firstName,
    lastName,
    fullName: () => {
      console.log(`${firstName} ${lastName}`)
    },
  }
}
//Driver Code 
newFunction('William', 'Imoh').fullName() 

//soal no 3
const newObject = {
  firstName: 'Muhammad',
  lastName: 'Iqbal Mubarok',
  address: 'Jalan Ranamanyar',
  hobby: 'playing football',
};
const {firstName, lastName, address, hobby} = newObject
console.log(firstName, lastName, address, hobby)

//soal no 4
const west = ['Will', 'Chris', 'Sam', 'Holly']
const east = ['Gill', 'Brian', 'Noel', 'Maggie']
const combined = [...west, ...east]
console.log(combined)

//soal no 5
const planet = 'earth' 
const view = 'glass'
let before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`
console.log(before)