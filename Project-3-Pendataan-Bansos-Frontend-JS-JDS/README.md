# Project 3 Pendataan Bansos (Frontend JS Project) - From Jabar Digital Service

<table style="border:none;">
    <tr>
        <td>Nama</td>
        <td>:</td>
        <td>Aisyah Cinta Putri Wibawa</td>
    </tr>
    <tr>
        <td>Kelas</td>
        <td>:</td>
        <td>VueJS</td>
    </tr>
    <tr>
        <td>Link Deployment</td>
        <td>:</td>
        <td>
            <a href="https://project-pendanaan-bansos.netlify.app/" target="_blank" rel="noopener">Link</a>
        </td>
    </tr>
    <tr>
        <td>Link Demo</td>
        <td>:</td>
        <td>
            <a href="https://drive.google.com/file/d/1F3cuxHkklejerS1dKUyy-4byVmeXi2R2/view?usp=sharing" target="_blank" rel="noopener">Link</a>
        </td>
    </tr>
</table>

# Keterangan

### Desain Layout

Saya menerapkan desain layout simple dengan warna pastel dan textnya hitam agar User mudah untuk mengisi formulir pendataan bansos. Karena, usia kepala RW biasanya lebih dari 40-50 tahun.

### Formulir

Saya menggunakan desain formulir dengan beberapa penataan gaya dan validasi seperti menggunakan placeholder agar dapat memberikan arahan kepada pengguna supaya lebih jelas dalam memasukan data yang diminta dan ketika pengguna salah/kurang tepat dalam menginputkan data maka akan muncul peringatan sehingga pengguna dapat memperbaiki input data yang kurang tepat.

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
