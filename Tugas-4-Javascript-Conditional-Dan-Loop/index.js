// soal 1
var nilai = 75;
if(nilai >= 85){
    console.log("indeksnya A")
} else if (nilai >=75 && nilai <85){
    console.log("indeksnya B")
} else if (nilai >=65 && nilai <75){
    console.log("indeksnya C")
} else if (nilai >=55 && nilai <65){
    console.log("indeksnya D")
} else {
    console.log("indeksnya E")
}

// soal 2
var tanggal = 26;
var bulan = 11;
var tahun = 2002;
switch(bulan){
    case 1: {console.log(tanggal+' Januari '+tahun); break; }
    case 2: {console.log(tanggal+' Februari '+tahun); break; }
    case 3: {console.log(tanggal+' Maret '+tahun); break; }
    case 4: {console.log(tanggal+' April '+tahun); break; }  
    case 5: {console.log(tanggal+' Mei '+tahun); break; }
    case 6: {console.log(tanggal+' Juni '+tahun); break; }
    case 7: {console.log(tanggal+' Juli '+tahun); break; }
    case 8: {console.log(tanggal+' Agustus '+tahun); break; } 
    case 9: {console.log(tanggal+' September '+tahun); break; }
    case 10: {console.log(tanggal+' Oktober '+tahun); break; }
    case 11: {console.log(tanggal+' November '+tahun); break; }
    case 12: {console.log(tanggal+' Desember '+tahun); break; }}    

// soal 3
console.log('Output untuk n=3');
var n=3
var segitiga = '#';
for (var segitiga= "#";segitiga.length<=n;segitiga+= "#"){
    console.log(segitiga)
}

console.log('Output untuk n=7');
var n=7
var segitiga = '#';
for (var segitiga= "#";segitiga.length<=n;segitiga+= "#"){
    console.log(segitiga)
}

//soal 4
var m=10;
for (var i=1; i<=m; i++){
    console.log(i + " - I Love Programming");
    if (i % 3 === 0){
        console.log("===")
    }
}