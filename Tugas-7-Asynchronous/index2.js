var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise
readBooksPromise(10000, books[0])
  .then((response) => {
    readBooksPromise(7000, books[1])
  })
  readBooksPromise(5000, books[2])
  .then((response) => {
    readBooksPromise(1000, books[3])
  })
  .catch((error) => console.log(error))