//Hari ke-3 ||Tugas Pengenalan Javascript
//soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";
var kata1 = pertama.substring(0,4)
var kata2 = pertama.substring(12,18)
var kata3 = kedua.substring(0,7)
var kata4 = kedua.substring(8,18).toUpperCase(8,18)
console.log(kata1.concat(' ').concat(kata2).concat(' ').concat(kata3).concat(' ').concat(kata4))

//soal 2
var kataPertama = Number("10");
var kataKedua = Number("2");
var kataKetiga = Number("4");
var kataKeempat = Number("6");
var operasi3 = kataPertama+kataKedua*kataKetiga+kataKeempat
console.log(operasi3)

//soal 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14);  
var kataKetiga = kalimat.substring(15, 18);
var kataKeempat = kalimat.substring(19, 24);
var kataKelima = kalimat.substring(25, 31);

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);